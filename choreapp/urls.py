from django.conf.urls import patterns, include, url
from django.contrib import admin
from chores import views
from django.contrib.auth.decorators import login_required

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', login_required(views.ChoreList.as_view()), name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': "/"}),
    # url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^users/(?P<user>\w+)/$', views.user_registered),
    url(r'chore/(?P<pk>\d+)/$', views.UpdateChore.as_view(), name='update_chore'),
    url(r'complete/(?P<chore>\d+)/$', views.complete, name='complete'),
    url(r'leaderboard/$', views.UserList.as_view(), name='leaderboard')
    # url(r'rankings/', views.rankings, name='rankings')
)
