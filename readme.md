Install
=======

After running ```python manage.py syncdb```, be sure to log in to the app, create a superuser, and delete the default test user.
