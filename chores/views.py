from django.http import HttpResponse
from django.contrib.auth.models import User
from django.views.generic import ListView, UpdateView
from .models import Chore
from datetime import date
# from .tasks import reset_chore
from django.shortcuts import redirect
from django.contrib.auth import get_user_model


class ChoreList(ListView):
    template_name = "chores/chore_list.html"
    model = Chore
    context_object_name = "chores"

    def get_queryset(self):
        return Chore.objects.filter(completed=False)

    def post(self, request, *args, **kwargs):
#         print(request['POST'])
        return Chore.objects.filter(completed=False)


class UpdateChore(UpdateView):
    model = Chore
    initial = {'completed': True, 'date_completed': date.today()}
    # fields = ["completed_by"]
    context_object_name = "chore"


class UserList(ListView):
    template_name = "users/index.html"
    model = User
    context_object_name = "users"


def user_registered(request, user):
    message = "{0} has been registered.".format(user)
    return HttpResponse(message)


def complete(request, chore):
    user_name = request.POST['completed_by']
    completed_chore = Chore.objects.get(pk=chore)
    completed_chore.completed = True
    completed_chore.completed_by = user_name
    user_model = get_user_model()
    current_user = user_model.objects.get(username=user_name)
    current_user.score.points += completed_chore.points
    current_user.score.save()
    completed_chore.save()
    return redirect('home')
