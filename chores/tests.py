from django.test import TestCase
from django.test import Client
from .models import Chore
from django.contrib.auth.models import User


class ChoreTestCase(TestCase):
    def setUp(self):
        test_chore1 = Chore.objects.create(title='test1',
            description='test1 description', points=5, completed=False)
        test_chore2 = Chore.objects.create(title='test2',
            description='test2 description', points=5, completed=False)
        self.test1 = Chore.objects.get(title='test1')
        self.test2 = Chore.objects.get(title='test2')

    def test_chore_has_points(self):
        self.assertEqual(self.test1.points, 5)
        self.assertEqual(self.test2.points, 5)

    def test_chore_has_name(self):
        self.assertEqual(self.test1.title, 'test1')
        self.assertEqual(self.test2.title, 'test2')

    def test_chore_has_description(self):
        self.assertEqual(self.test1.description, 'test1 description')
        self.assertEqual(self.test2.description, 'test2 description')

    def test_chore_completed(self):
        self.assertEqual(self.test1.completed, False)
        self.assertEqual(self.test2.completed, False)


class TestRootView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(
            'testuser', 'test@test.com', 'testpass')

    def test_must_login(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 302)

    def test_user_login(self):
        self.client.login(username='testuser', password='testpass')
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)


class TestChoreCompletion(TestCase):
    def setUp(self):
        self.client = Client()
        self.test_chore = Chore.objects.create(
            title='testcompletion', description='test completion',
            points=5)
        self.user = User.objects.create_user(
            'testuser', 'test@test.com', 'testpass')


class TestUserPoints(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            'testuser', 'test@test.com', 'testpass')

    def test_initial_score_is_zero(self):
        self.assertEqual(self.user.score.points, 0)

    def test_adding_to_user_score(self):
        self.user.score.points += 5
        self.assertEqual(self.user.score.points, 5)
