from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.conf import settings


class Chore(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField()
    date_completed = models.DateField(blank=True, null=True)
    date_added = models.DateField(auto_now_add=True)
    points = models.IntegerField(default=0)
    completed = models.BooleanField(blank=True, default=False)
    completed_by = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return "{0}".format(self.title)

    def get_absolute_url(self):
        return reverse('home')


class Score(models.Model):
    user = models.OneToOneField(User, unique=True)
    points = models.IntegerField(default=0)

    def __str__(self):
        return "{0}".format(self.points)

def create_user_score(sender, instance, **kwargs):
    user_score = Score.objects.get_or_create(user=instance)
    # user_score.save()


models.signals.post_save.connect(create_user_score , sender=User)
