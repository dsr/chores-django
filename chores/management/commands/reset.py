from chores.models import Chore
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Reset all chores' completed status"

    def handle(self, *args, **kwargs):
        chores = Chore.objects.all()
        for chore in chores:
            chore.completed = False
            chore.save()
        self.stdout.write("Reset all chores.")