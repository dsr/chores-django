from django.contrib import admin
from .models import Chore
from .models import Score
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin



class ScoreInline(admin.StackedInline):
    model = Score
    can_delete = False
    verbose_plural_name = 'points'


class ChoreAdmin(admin.ModelAdmin):
    list_display = ('title', 'completed', 'completed_by')
    list_display_links = ['title']
    list_editable = ['completed_by', 'completed']


class UserAdmin(UserAdmin):
    inlines = (ScoreInline,)
    list_display = ('username', 'score')


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Chore, ChoreAdmin)


